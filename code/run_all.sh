#!/bin/bash

if [ $# -ne 1 ]; then
	echo usage: $0 '<square-side-resolution>'
	exit 1
fi

./exercise_1 -screen $1 $1 60 -fsaa 4 -frames 8000 -app anim_step 0.0125 &
./exercise_2 -screen $1 $1 60 -fsaa 4 -frames 8000 -app anim_step 0.0125 -app albedo_map navy_blue_scrapbook_paper.raw 8 8 -app normal_map chesterfield.raw 8 8 &
./exercise_3 -screen $1 $1 60 -fsaa 4 -frames 8000 -app anim_step 0.0125 -app albedo_map default 128 64 &
./exercise_4 -screen $1 $1 60 -fsaa 4 -frames 8000 -app anim_step 0.0125 -app shadow_res 1024 &
./exercise_5 -screen $1 $1 60 -fsaa 4 -frames 8000 -app anim_step 0.0125 -app mesh Axonn.mesh -app rot_axes 0 1 0 &
./exercise_6 -screen $1 $1 60         -frames 8000 -app anim_step 0.0125 &
