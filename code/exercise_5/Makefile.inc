SRCS += \
	$(EXTERN_EXERCISE_PATH)/app_mesh.cpp \
	$(EXTERN_COMMON_PATH)/rendIndexedTrilist.cpp \
	$(EXTERN_COMMON_PATH)/util_file.cpp \
	$(EXTERN_COMMON_PATH)/util_misc.cpp

CFLAGS += \
	-Wno-maybe-uninitialized \
	-DMESH_INDEX_START=1
