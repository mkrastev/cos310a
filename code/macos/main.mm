/*
 Copyright (C) 2015 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 Standard AppKit entry point.
 */

#import <Cocoa/Cocoa.h>
#import "AppDelegate.h"
#import "param.h"
#import "stream.hpp"

int main(int argc, char * argv[])
{
	// set up cin, cout and cerr substitute streams
	stream::cin.open(stdin);
	stream::cout.open(stdout);
	stream::cerr.open(stderr);

#if FB_RES_FIXED_W
	param.image_w = FB_RES_FIXED_W;

#else
	param.image_w = 1024;

#endif
#if FB_RES_FIXED_H
	param.image_h = FB_RES_FIXED_H;

#else
	param.image_h = 1024;

#endif
	param.bitness[0] = 8;
	param.bitness[1] = 8;
	param.bitness[2] = 8;
	param.bitness[3] = 0;
	param.fsaa = 0;
	param.frames = -1U;

	// read render setup from CLI
	const int result_cli = parseCLI(argc, argv, &param);

	if (0 != result_cli)
		return result_cli;

	@autoreleasepool {
		NSApplication *application = [NSApplication sharedApplication];
		[application activateIgnoringOtherApps: YES];

		// provide app menu with one item: quit
		NSMenuItem *item = [[NSMenuItem alloc] init];
		[item setSubmenu: [[NSMenu alloc] init]];
		[item.submenu addItem: [[NSMenuItem alloc] initWithTitle: [@"Quit " stringByAppendingString: NSProcessInfo.processInfo.processName] action:@selector(terminate:) keyEquivalent:@"q"]];
		[application setMainMenu: [[NSMenu alloc] init]];
		[application.mainMenu addItem: item];

		AppDelegate *appDelegate = [[AppDelegate alloc] init];
		[application setDelegate: appDelegate];
		[application run];
	}
	return EXIT_SUCCESS;
}
