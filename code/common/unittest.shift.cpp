#include <iomanip>
#include "timer.h"
#include "stream.hpp"
#include "vectnative.hpp"

#ifdef _WIN32
#include <SDKDDKVer.h>
#include <stdio.h>

#else
// verify iostream-free status (see further down)
#if _GLIBCXX_IOSTREAM != 0
#error rogue iostream acquired
#endif

#endif
// use a custom minimalistic implementation of std::istream/ostream to avoid the highly-harmful-for-the-optimisations
// static initialization of std::cin/cout/cerr; our mini-implementation is initialized at the start of main()
namespace stream {
in cin;
out cout;
out cerr;
}

#if _MSC_VER != 0
using namespace simd; // oldest msvc we target does not support using-declarations, so we go full monty there
#else
#if NATIVE_F64X4 != 0
using simd::f64x4;
using simd::as_f64x4;
using simd::get_f64x2;
#endif
#if NATIVE_S64X4 != 0
using simd::s64x4;
using simd::as_s64x4;
using simd::get_s64x2;
#endif
#if NATIVE_U64X4 != 0
using simd::u64x4;
using simd::as_u64x4;
using simd::get_u64x2;
#endif
#if NATIVE_F32X8 != 0
using simd::f32x8;
using simd::as_f32x8;
using simd::get_f32x4;
#endif
#if NATIVE_S32X8 != 0
using simd::s32x8;
using simd::as_s32x8;
using simd::get_s32x4;
#endif
#if NATIVE_U32X8 != 0
using simd::u32x8;
using simd::as_u32x8;
using simd::get_u32x4;
#endif
#if NATIVE_F32X2 != 0
using simd::f32x2;
using simd::as_f32x2;
#endif
#if NATIVE_S32X2 != 0
using simd::s32x2;
using simd::as_s32x2;
#endif
#if NATIVE_U32X2 != 0
using simd::u32x2;
using simd::as_u32x2;
#endif
#if NATIVE_S16X16 != 0
using simd::s16x16;
using simd::as_s16x16;
using simd::get_s16x8;
#endif
#if NATIVE_U16X16 != 0
using simd::u16x16;
using simd::as_u16x16;
using simd::get_u16x8;
#endif
#if NATIVE_S16X4 != 0
using simd::s16x4;
using simd::as_s16x4;
#endif
#if NATIVE_U16X4 != 0
using simd::u16x4;
using simd::as_u16x4;
#endif
using simd::f64x2;
using simd::s64x2;
using simd::u64x2;
using simd::f32x4;
using simd::s32x4;
using simd::u32x4;
using simd::s16x8;
using simd::u16x8;
using simd::as_f64x2;
using simd::as_s64x2;
using simd::as_u64x2;
using simd::as_f32x4;
using simd::as_s32x4;
using simd::as_u32x4;
using simd::as_s16x8;
using simd::as_u16x8;
using simd::all;
using simd::any;
using simd::none;
using simd::min;
using simd::max;
using simd::abs;
using simd::mask;
using simd::select;
using simd::sqrt;
using simd::log;
using simd::exp;
using simd::pow;
using simd::sin;
using simd::cos;
using simd::sincos;
using simd::shl;
using simd::shr;
using simd::transpose4x4;
using simd::operator ==;
using simd::operator !=;
using simd::operator <;
using simd::operator >;
using simd::operator <=;
using simd::operator >=;
using simd::flag_zero;
using simd::flag_native;
#endif

template < typename T >
const T& min(const T& a, const T& b) {
	return (a < b) ? a : b;
}

template < typename T >
const T& max(const T& a, const T& b) {
	return (a > b) ? a : b;
}

class formatter_f64 {
	double f;

public:
	formatter_f64(const double a) : f(a) {}

	uint64_t get() const {
		return reinterpret_cast< const uint64_t& >(f);
	}
};

class formatter_f32 {
	float f;

public:
	formatter_f32(const float a) : f(a) {}

	uint32_t get() const {
		return reinterpret_cast< const uint32_t& >(f);
	}
};

static stream::out& operator << (
	stream::out& str,
	const formatter_f64 a) {

	str << 
		(a.get() >> 63) << ':' <<
		std::setw(3)  << std::setfill('0') << (a.get() >> 52 & 0x7ff) << ':' <<
		std::setw(13) << std::setfill('0') << (a.get() >>  0 & 0xfffffffffffff);
	return str;
}

static stream::out& operator << (
	stream::out& str,
	const formatter_f32 a) {

	str << 
		(a.get() >> 31) << ':' <<
		std::setw(2) << std::setfill('0') << (a.get() >> 23 & 0xff) << ':' <<
		std::setw(6) << std::setfill('0') << (a.get() >>  0 & 0x7fffff);
	return str;
}

#if NATIVE_F64X4 != 0
static stream::out& operator << (
	stream::out& str,
	const f64x4& a) {

	return str <<
		formatter_f64(a[0]) << ' ' << formatter_f64(a[1]) << ' ' << formatter_f64(a[2]) << ' ' << formatter_f64(a[3]);
}

#endif
#if NATIVE_S64X4 != 0
static stream::out& operator << (
	stream::out& str,
	const s64x4& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3];
}

#endif
#if NATIVE_U64X4 != 0
static stream::out& operator << (
	stream::out& str,
	const u64x4& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3];
}

#endif
static stream::out& operator << (
	stream::out& str,
	const f64x2& a) {

	return str <<
		formatter_f64(a[0]) << ' ' << formatter_f64(a[1]);
}

static stream::out& operator << (
	stream::out& str,
	const s64x2& a) {

	return str <<
		a[0] << ' ' << a[1];
}

static stream::out& operator << (
	stream::out& str,
	const u64x2& a) {

	return str <<
		a[0] << ' ' << a[1];
}

#if NATIVE_F32X8 != 0
static stream::out& operator << (
	stream::out& str,
	const f32x8& a) {

	return str <<
		formatter_f32(a[0]) << ' ' << formatter_f32(a[1]) << ' ' << formatter_f32(a[2]) << ' ' << formatter_f32(a[3]) << ' ' <<
		formatter_f32(a[4]) << ' ' << formatter_f32(a[5]) << ' ' << formatter_f32(a[6]) << ' ' << formatter_f32(a[7]);
}

#endif
#if NATIVE_S32X8 != 0
static stream::out& operator << (
	stream::out& str,
	const s32x8& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3] << ' ' <<
		a[4] << ' ' << a[5] << ' ' << a[6] << ' ' << a[7];
}

#endif
#if NATIVE_U32X8 != 0
static stream::out& operator << (
	stream::out& str,
	const u32x8& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3] << ' ' <<
		a[4] << ' ' << a[5] << ' ' << a[6] << ' ' << a[7];
}

#endif
static stream::out& operator << (
	stream::out& str,
	const f32x4& a) {

	return str <<
		formatter_f32(a[0]) << ' ' << formatter_f32(a[1]) << ' ' << formatter_f32(a[2]) << ' ' << formatter_f32(a[3]);
}

static stream::out& operator << (
	stream::out& str,
	const s32x4& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3];
}

static stream::out& operator << (
	stream::out& str,
	const u32x4& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3];
}

#if NATIVE_F32X2 != 0
static stream::out& operator << (
	stream::out& str,
	const f32x2& a) {

	return str <<
		formatter_f32(a[0]) << ' ' << formatter_f32(a[1]);
}

#endif
#if NATIVE_S32X2 != 0
static stream::out& operator << (
	stream::out& str,
	const s32x2& a) {

	return str <<
		a[0] << ' ' << a[1];
}

#endif
#if NATIVE_U32X2 != 0
static stream::out& operator << (
	stream::out& str,
	const u32x2& a) {

	return str <<
		a[0] << ' ' << a[1];
}

#endif
#if NATIVE_S16X16 != 0
static stream::out& operator << (
	stream::out& str,
	const s16x16& a) {

	return str <<
		a[ 0] << ' ' << a[ 1] << ' ' << a[ 2] << ' ' << a[ 3] << ' ' <<
		a[ 4] << ' ' << a[ 5] << ' ' << a[ 6] << ' ' << a[ 7] << ' ' <<
		a[ 8] << ' ' << a[ 9] << ' ' << a[10] << ' ' << a[11] << ' ' <<
		a[12] << ' ' << a[13] << ' ' << a[14] << ' ' << a[15];
}

#endif
#if NATIVE_U16X16 != 0
static stream::out& operator << (
	stream::out& str,
	const u16x16& a) {

	return str <<
		a[ 0] << ' ' << a[ 1] << ' ' << a[ 2] << ' ' << a[ 3] << ' ' <<
		a[ 4] << ' ' << a[ 5] << ' ' << a[ 6] << ' ' << a[ 7] << ' ' <<
		a[ 8] << ' ' << a[ 9] << ' ' << a[10] << ' ' << a[11] << ' ' <<
		a[12] << ' ' << a[13] << ' ' << a[14] << ' ' << a[15];
}

#endif
static stream::out& operator << (
	stream::out& str,
	const s16x8& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3] << ' ' <<
		a[4] << ' ' << a[5] << ' ' << a[6] << ' ' << a[7];
}

static stream::out& operator << (
	stream::out& str,
	const u16x8& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3] << ' ' <<
		a[4] << ' ' << a[5] << ' ' << a[6] << ' ' << a[7];
}

#if NATIVE_S16X4 != 0
static stream::out& operator << (
	stream::out& str,
	const s16x4& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3];
}

#endif
#if NATIVE_U16X4 != 0
static stream::out& operator << (
	stream::out& str,
	const u16x4& a) {

	return str <<
		a[0] << ' ' << a[1] << ' ' << a[2] << ' ' << a[3];
}

#endif
// compute the ULP difference between two finite f32's in FTZ mode, up to 24 orders of magnitude
// (larger difference is clamped to 24 orders)
int32_t diff_ulp_ftz(const float a, const float b) {
	const uint32_t ia = reinterpret_cast< const uint32_t& >(a);
	const uint32_t ib = reinterpret_cast< const uint32_t& >(b);
	const uint32_t sign_mask = 0x80000000;
	const uint32_t exp_mask = 0x7f800000;
	const uint32_t mant_mask = ~(sign_mask | exp_mask);

	// early out at identical args or different-sign zeros
	if (ia == ib || sign_mask == (ia | ib))
		return 0;

	// don't bother with a single zero or different-sign non-zero args
	if (0 == (ia & ~sign_mask) || 0 == (ib & ~sign_mask) || (ia & sign_mask) != (ib & sign_mask))
		return -1;

	const uint32_t maxar = max(ia, ib);
	const uint32_t minar = min(ia, ib);
	const uint32_t maxar_exp = maxar >> 23 & 0xff;
	const uint32_t minar_exp = minar >> 23 & 0xff;
	const uint32_t exp_diff = maxar_exp - minar_exp;

	// make sure our same-scale difference fits 64 bits
	if (40 < exp_diff)
		return 1 << 24;

	const uint64_t same_scale_maxar = uint64_t(0x800000 | maxar & mant_mask) << exp_diff;
	const uint64_t same_scale_minar = uint64_t(0x800000 | minar & mant_mask);
	const uint64_t diff = same_scale_maxar - same_scale_minar;

	// clamp differences to 24 orders of magnitude
	return uint32_t(min(diff, uint64_t(1 << 24)));
}

// compute the ULP difference between two finite f32's, up to 24 orders of magnitude
// (larger difference is clamped to 24 orders)
int32_t diff_ulp(const float a, const float b) {
	const uint32_t ia = reinterpret_cast< const uint32_t& >(a);
	const uint32_t ib = reinterpret_cast< const uint32_t& >(b);
	const uint32_t sign_mask = 0x80000000;
	const uint32_t exp_mask = 0x7f800000;
	const uint32_t mant_mask = ~(sign_mask | exp_mask);

	// early out at identical args or different-sign zeros
	if (ia == ib || sign_mask == (ia | ib))
		return 0;

	// don't bother with a single zero or different-sign non-zero args
	if (0 == (ia & ~sign_mask) || 0 == (ib & ~sign_mask) || (ia & sign_mask) != (ib & sign_mask))
		return -1;

	const uint32_t maxar = max(ia, ib);
	const uint32_t minar = min(ia, ib);
	const uint32_t maxar_exp = maxar >> 23 & 0xff;
	const uint32_t minar_exp = minar >> 23 & 0xff;
	const uint32_t maxar_msb = maxar_exp ? 0x800000 : 0;
	const uint32_t minar_msb = minar_exp ? 0x800000 : 0;
	const uint32_t exp_diff = (maxar_exp ? maxar_exp : 1) - (minar_exp ? minar_exp : 1);

	// make sure our same-scale difference fits 64 bits
	if (40 < exp_diff)
		return 1 << 24;

	const uint64_t same_scale_maxar = uint64_t(maxar_msb | maxar & mant_mask) << exp_diff;
	const uint64_t same_scale_minar = uint64_t(minar_msb | minar & mant_mask);
	const uint64_t diff = same_scale_maxar - same_scale_minar;

	// clamp differences to 24 orders of magnitude
	return uint32_t(min(diff, uint64_t(1 << 24)));
}

float compose_f32(const uint32_t sign, const uint32_t exp, const uint32_t mant) {
	const uint32_t ret = (sign << 31) | ((exp & 0xff) << 23) | (mant & 0x7fffff);
	return reinterpret_cast< const float& >(ret);
}

#if NATIVE_S64X4 != 0
s64x4 s64x4_0[1] __attribute__ ((aligned(64))) = {
	s64x4(1, 2, 4, 8)
};
u64x4 u64x4_1[1] __attribute__ ((aligned(64))) = {
	u64x4(0, 1, 2, 2)
};
s64x4 s64x4_r[2] __attribute__ ((aligned(64)));
s64x4 s64x4_s[2] __attribute__ ((aligned(64)));

#endif
s64x2 s64x2_0[1] __attribute__ ((aligned(64))) = {
	s64x2(1, 2)
};
u64x2 u64x2_1[1] __attribute__ ((aligned(64))) = {
	u64x2(0, 1)
};
s64x2 s64x2_r[2] __attribute__ ((aligned(64)));
s64x2 s64x2_s[2] __attribute__ ((aligned(64)));

#if NATIVE_S32X8 != 0
s32x8 s32x8_0[1] __attribute__ ((aligned(64))) = {
	s32x8(1, 2, 3, 4, 5, 6, 7, 8)
};
u32x8 u32x8_1[1] __attribute__ ((aligned(64))) = {
	u32x8(1, 0, 0, 0, 2, 2, 2, 2)
};
s32x8 s32x8_r[2] __attribute__ ((aligned(64)));
s32x8 s32x8_s[2] __attribute__ ((aligned(64)));

#endif
s32x4 s32x4_0[1] __attribute__ ((aligned(64))) = {
	s32x4(1, 2, 3, 4)
};
u32x4 u32x4_1[1] __attribute__ ((aligned(64))) = {
	u32x4(1, 0, 2, 2)
};
s32x4 s32x4_r[2] __attribute__ ((aligned(64)));
s32x4 s32x4_s[2] __attribute__ ((aligned(64)));

#if NATIVE_S16X16 != 0
s16x16 s16x16_0[1] __attribute__ ((aligned(64))) = {
	s16x16(1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8)
};
u16x16 u16x16_1[1] __attribute__ ((aligned(64))) = {
	u16x16(1, 0, 0, 0, 2, 2, 2, 2, 1, 0, 0, 0, 2, 2, 2, 2)
};
s16x16 s16x16_r[2] __attribute__ ((aligned(64)));
s16x16 s16x16_s[2] __attribute__ ((aligned(64)));

#endif
s16x8 s16x8_0[1] __attribute__ ((aligned(64))) = {
	s16x8(1, 2, 3, 4, 5, 6, 7, 8)
};
u16x8 u16x8_1[1] __attribute__ ((aligned(64))) = {
	u16x8(1, 0, 0, 0, 2, 2, 2, 2)
};
s16x8 s16x8_r[2] __attribute__ ((aligned(64)));
s16x8 s16x8_s[2] __attribute__ ((aligned(64)));

int main(int argc, char**) {
	const size_t obfuscate = size_t(argc - 1);

	stream::cin.open(stdin);
	stream::cout.open(stdout);
	stream::cerr.open(stderr);

	stream::cout << std::hex;
	stream::cerr << std::hex;
	bool success = true;

	const size_t reps = 1e9;

	size_t ti = 0;
	uint64_t time[32];
	time[0] = timer_ns();

	// left shift //////////////////////////////////////////////////////////////////////////////////

#if NATIVE_S32X8 != 0
	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s32x8_r[0 + offset] = shl(s32x8_0[offset], u32x8_1[offset]);
	}

	time[++ti] = timer_ns();

	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s32x8_r[1 + offset] = s32x8::generic(s32x8_0[offset]) << s32x8::generic(as_s32x8(u32x8_1[offset]));
	}

	time[++ti] = timer_ns();

#endif
	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s32x4_r[0 + offset] = shl(s32x4_0[offset], u32x4_1[offset]);
	}

	time[++ti] = timer_ns();

	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s32x4_r[1 + offset] = s32x4::generic(s32x4_0[offset]) << s32x4::generic(as_s32x4(u32x4_1[offset]));
	}

	time[++ti] = timer_ns();

#if NATIVE_S16X16 != 0
	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s16x16_r[0 + offset] = shl(s16x16_0[offset], u16x16_1[offset]);
	}

	time[++ti] = timer_ns();

	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s16x16_r[1 + offset] = s16x16::generic(s16x16_0[offset]) << s16x16::generic(as_s16x16(u16x16_1[offset]));
	}

	time[++ti] = timer_ns();

#endif
	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s16x8_r[0 + offset] = shl(s16x8_0[offset], u16x8_1[offset]);
	}

	time[++ti] = timer_ns();

	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s16x8_r[1 + offset] = s16x8::generic(s16x8_0[offset]) << s16x8::generic(as_s16x8(u16x8_1[offset]));
	}

	time[++ti] = timer_ns();

	// right shift /////////////////////////////////////////////////////////////////////////////////

#if NATIVE_S64X4 != 0
	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s64x4_s[0 + offset] = shr< 2 >(s64x4_0[offset]);
	}

	time[++ti] = timer_ns();

	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s64x4_s[1 + offset] = s64x4::generic(s64x4_0[offset]) >> (s64x4::generic) { 2, 2, 2, 2 };
	}

	time[++ti] = timer_ns();

#endif
	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s64x2_s[0 + offset] = shr< 1 >(s64x2_0[offset]);
	}

	time[++ti] = timer_ns();

	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s64x2_s[1 + offset] = s64x2::generic(s64x2_0[offset]) >> (s64x2::generic) { 1, 1 };
	}

	time[++ti] = timer_ns();

	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s32x4_s[0 + offset] = shr(s32x4_0[offset], u32x4_1[offset]);
	}

	time[++ti] = timer_ns();

	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s32x4_s[1 + offset] = s32x4::generic(s32x4_0[offset]) >> s32x4::generic(as_s32x4(u32x4_1[offset]));
	}

	time[++ti] = timer_ns();

	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s16x8_s[0 + offset] = shr(s16x8_0[offset], u16x8_1[offset]);
	}

	time[++ti] = timer_ns();

	for (size_t i = 0; i < reps; ++i) {
		const size_t offset = i * obfuscate;

		s16x8_s[1 + offset] = s16x8::generic(s16x8_0[offset]) >> s16x8::generic(as_s16x8(u16x8_1[offset]));
	}

	time[++ti] = timer_ns();

	// left shift //////////////////////////////////////////////////////////////////////////////////

#if NATIVE_S32X8 != 0
	stream::cout << s32x8_r[0] << '\n';
	stream::cout << s32x8_r[1] << '\n';

#endif
	stream::cout << s32x4_r[0] << '\n';
	stream::cout << s32x4_r[1] << '\n';

#if NATIVE_S16X16 != 0
	stream::cout << s16x16_r[0] << '\n';
	stream::cout << s16x16_r[1] << '\n';

#endif
	stream::cout << s16x8_r[0] << '\n';
	stream::cout << s16x8_r[1] << '\n';

	// right shift /////////////////////////////////////////////////////////////////////////////////

#if NATIVE_S64X4 != 0
	stream::cout << s64x4_s[0] << '\n';
	stream::cout << s64x4_s[1] << '\n';

#endif
	stream::cout << s64x2_s[0] << '\n';
	stream::cout << s64x2_s[1] << '\n';

	stream::cout << s32x4_s[0] << '\n';
	stream::cout << s32x4_s[1] << '\n';

	stream::cout << s16x8_s[0] << '\n';
	stream::cout << s16x8_s[1] << '\n';

	for (size_t i = 0; i < ti; ++i)
		stream::cout << "elapsed: " << (time[i + 1] - time[i]) / 1e9 << '\n';

	return success ? 0 : -1;
}
