#ifndef native_gl_H__
#define native_gl_H__

#if __APPLE__
#include <OpenGL/gl3.h>
#elif _WIN32
#include <GL/glew.h>
#else
#include <GL/gl.h>
#endif

#endif // native_gl_H__
