#!/bin/bash

if [ $# -ne 1 ]; then
	echo usage: $0 '<square-side-resolution>'
	exit 1
fi

./exercise_3 -screen $1 $1 60 -fsaa 16 -frames 8000 -app albedo_map rockwall_albedo.raw 8 8 -app normal_map rockwall_normal.raw 8 8 -app tile 4 &
./exercise_3 -screen $1 $1 60 -fsaa 16 -frames 8000 -app albedo_map none 128 64 &
