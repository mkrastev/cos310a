# COS 310 Topics in Computer Science: Intro to Computer Graphics ‒ code, lecture slides and supplementary teaching material

Course Schedule
===============

* module 1: The Notion of Color in Computer Systems [pdf](slides/Module1_Color.pdf)
* module 2: The Anatomy of the GPU ‒ CPU vs GPU Juxtaposition [pdf](slides/Module2_GPU.pdf)
* module 3: Homogeneous Coordinates and Transforms ‒ The Model-View-Projection Paradigm [pdf](slides/Module3_MVP.pdf)
* module 4: Reflectance Properties of Materials ‒ BRDF [pdf](slides/Module4_BRDF.pdf)
* module 5: Light Occlusion in Rasterization ‒ Shadows [pdf](slides/Module5_Shadows.pdf)
* module 6: Ray Tracing vs Rasterization ‒ Physical Principles [pdf](slides/Module6_RayTracing.pdf)
 
Code Directory-Tree Structure
=============================

The `./code` subtree contains all code for the course exercises:

* common     ‒ code shared among all exercises ‒ wrappers, helpers and misc utilities required to build the exercises
* exercise_N ‒ each individual exercise, where `N` spans the exercise order
* macos      ‒ platform-specific code for building an exercise on Apple macOS (v10.10 and higher); requires the Xcode command-line tools
* glut       ‒ platform-specific code for building an exercise on GLUT-enabled Unices; requires FreeGLUT (or any other GLUT with 3.2 core-profile capabilities), c++11-capable g++/clang and GNU make.

Platform-specific directories should each contain a Makefile and a build script `build.sh`. The later uses the former to build an exercise, then collects all artifacts needed to run the exercise in the same platform-specific directory.

**Note**: all Makefiles have their compilers and linkers hardcoded in CC and LD variables; change those according to your preferences and compiler availability.

Bulding on MinGW
================

GLUT platform support allows building the exercises on MinGW. For the purpose you need:

* cmake
* mingw32
* freeGLUT
* GLEW

Installing CMake
----------------
Download and run the MSI installer from [the official download page](https://cmake.org/download/).

Installing MinGW32
------------------

Installer `mingw-get-setup.exe` can be downloaded from [the official site](http://www.mingw.org/). Running the installer eventually provides a package selection dialogue. Make sure to select:

* mingw-developer-toolkit
* mingw32-base
* mingw32-gcc-g++
* msys-base

Followed by 'Apply Changes' in the 'Installation' menu. By default, mingw32 should install in `c:\MinGW`; mingw32 CLI environment is launched via `c:\MinGW\msys\1.0\msys.bat`.

Installing FreeGLUT
-------------------

Download the tarball archive for [freeGLUT-3.0.0](http://freeglut.sourceforge.net/index.php#download) and unpack that in the parent folder of your cos310a repo (i.e. one level up the tree from the repo). Open the header `freeglut-3.0.0/include/GL/freeglut_std.h` and either remove or comment out all inclusions of header `GL/glu.h`. Next, build the *Release Win32* target of FreeGLUT *without its demos* in a build folder named `freeglut_build` at the same level: first create an MSVS project via `cmake ../freeglut-3.0.0 -DFREEGLUT_BUILD_DEMOS=OFF`, then build configuration *Release Win32* via MSVS IDE.

Installing GLEW
---------------

Download the tarball archive for [glew-2.1.0](http://glew.sourceforge.net/) and unpack that in the parent folder of your cos310a. At the end of this step your cos310a repo parent folder should have the following content (as a minimum):

* cos310a
* freeglut-3.0.0
* freeglut_build
* glew-2.1.0

At this stage you can change directory to `cos310a/code` and invoke `./build.sh`.

Recommended readings
====================

* **Eric Lengyel**, Charles River Media Inc, 2002, *Mathematics for 3D Game Programming and Computer Graphics*
* **Alan Watt and Mark Watt**, ACM Press, 1992, *Advanced Animation and Rendering Techniques* 
* **George Wolberg**, IEEE Computer Society Press, 1990, *Digital Image Warping*
* **David H. Eberly**, Morgan Kaufmann, 2001, *3D Game Engine Design: A Practical Approach to Real-Time Computer Graphics*
* **Jason Gregory**, CRC Press, 2014, *Game Engine Architecture*
